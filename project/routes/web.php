<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app');
});

Route::get('/googlefd702c1ceff38ce9.html', function () {
    return "google-site-verification: googlefd702c1ceff38ce9.html";
});

Route::post('/contact', 'ContactUsController');