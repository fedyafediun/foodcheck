const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .copy('resources/images/', 'public/images', false)
    .less('node_modules/bootstrap/less/bootstrap.less', 'public/css')
    .sass('resources/styles/app.scss', 'public/css')
    // .styles([
    //     'public/css/bootstrap.css'
    // ], 'public/vendor.css')
    .js('resources/js/main.js', 'public/js')
