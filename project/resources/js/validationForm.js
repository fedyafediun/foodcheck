(function ($) {

    /*==================================================================
       [ Focus Contact2 ]*/
    $('.input100').each(function () {
        $(this).on('blur', function () {
            if ($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })
    })

    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit', function (event) {


        var form_is_valid = true;

        for (var i = 0; i < input.length; i++) {
            if (validate(input[i]) == false) {
                showValidate(input[i]);
                form_is_valid = false;
            }
        }

        if (!form_is_valid) {
            return false;
        }

        event.preventDefault();

        var button = $('.contact100-form-btn'),
            spinner = '<span class="spinner"></span>';

        if (!button.hasClass('loading')) {
            button.toggleClass('loading');
            button.html(spinner);
            button.attr("disabled", "disabled");
        }
        else {
            button.toggleClass('loading').html("Отправить");
            button.removeAttr("disabled")
        }

        $.ajax({
            type: 'POST',
            url: '/contact' ,
            data: $('.validate-form').serialize(),
            success: function(data) {
                console.log(data, 'success');
                button.toggleClass('loading').html("Отправить");
                button.removeAttr("disabled");
                $('#contact_form').trigger("reset");
            },
            error() {
                button.toggleClass('loading').html("Отправить");
                button.removeAttr("disabled")
            }
        },)
    });


    $('.validate-form .input100').each(function () {
        $(this).focus(function () {
            hideValidate(this);
        });
    });

    function validate(input) {
        if ($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if ($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if ($(input).val().trim() == '') {
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
})(jQuery);