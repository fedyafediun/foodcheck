jQuery(document).ready(function () {
    $(window).on('load', function () {
        setTimeout(function () {
            $(".loading-text").fadeOut();
            $(".loading").delay(350).fadeOut("slow");
        }, 1000);

        if ($(window).width() <= 780 || $(".navbar-default").offset().top > 50) {
            $(".reveal-menu-home").addClass("sticky-nav");
            $(".reveal-menu-blog").addClass("sticky-nav-white");
        }

    });

    $('body').scrollspy({target: '#navbar-spy', offset: 50})

    $("#navbar-spy li a[href^='#']").on('click', function(event) {
        $('#navbar-spy').collapse('hide')
    });

    $(window).on('scroll', function () {

        if ($(window).width() <= 780 || $(".navbar-default").add(".navbar-inverse").offset().top > 50) {
            $(".reveal-menu-home").addClass("sticky-nav");
            $(".reveal-menu-blog").addClass("sticky-nav-white");
        } else {
            $(".reveal-menu-home").removeClass("sticky-nav");
            $(".reveal-menu-blog").removeClass("sticky-nav-white");
        }
    });

    $('#radio-f').on('change', (event) => {
        if (!event.target.checked) {
            return false;
        }

        $('html, body').animate({ scrollTop: $('#questions').offset().top }, 1500);
    })
});



