<div id="our_target">
    <div class="container">
        <div class="block-header row">
            <div class=" col-xs-12 col-sm-12 col-md-3 vcenter">
                <h2 class="block-header-title mt-none mb-none">Наша цель</h2>
            </div>
            <div class="col-md-1 vcenter" style="width: 6.33%">
                <div class="block-header-dot hidden-xs hidden-sm " ></div>
            </div>
            <div class="col-xs-12 col-md-8 vcenter">
                <p class="block-header-description mb-none">
                    Мы понимаем насколько важно получить еду, которая совпадает с картинкой заказа.
                    Наша цель - сделать Ваш опыт заказа еды онлайн лучше,
                    предоставить возможность сравнить сервис заведений и качество доставляемой продукции.
                    Помогите нам узнать интересно ли Вам будет пользоваться таким ресурсом.
                </p>
            </div>
        </div>

        <div class="row sushi-set-img">
            <div class="col-xs-12">
                <img src="../../images/about_us.png" class="w-full" alt="" >
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3 col-md-3 col-xs-6 ">
                <div class="thumbnail">
                    <img src="../../images/salmon.png" alt="">
                    <div class="caption">
                        <p>Свежесть продукта подтверждена</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-md-3 col-xs-6 ">
                <div class="thumbnail">
                    <img src="../../images/caviar.png"  alt="">
                    <div class="caption">
                        <p>Свежесть продукта подтверждена</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-md-3 col-xs-6 ">
                <div class="thumbnail">
                    <img src="../../images/philadelphia.png" alt="">
                    <div class="caption">
                        <p>Продукт ожидает проверку</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-md-3 col-xs-6 ">
                <div class="thumbnail">
                    <img src="../../images/avocado.png" alt="">
                    <div class="caption">
                        <p>Свежесть продукта подтверждена</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="block-header row" style="margin-top: 60px">
            <div class=" col-xs-12 col-sm-12 col-md-4 vcenter">
                <h2 class="block-header-title mt-none mb-none">Процедура оценки</h2>
            </div>
            <div class="col-md-1 vcenter" style="width: 6.33%">
                <div class="block-header-dot hidden-xs hidden-sm"></div>
            </div>
            <div class="col-xs-12 col-md-7 vcenter">
                <p class="block-header-description mb-none">
                    Наша команда готова предоставить вам полностью всю необходимую информацию
                    по доставкам Суши в городе Киев. Вы сможете сравнить как будет выглядеть
                    одно и то же блюдо доставленное из разных заведений и дополнительно дать свою
                    оценку и комментарий, что поможет нам более обьективно сформировать рейтинг
                    доставок!
                </p>
            </div>
        </div>


        <div class="row" style="padding-bottom: 60px">
            <div class="col-xs-12">
                <img src="../../images/foodRating.png" class="w-full" alt="" >
            </div>
        </div>

    </div>
</div>