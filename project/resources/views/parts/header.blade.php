<header id="rt_header">
    <!-- Navigation Bar -->
    <nav>
        <div class="navbar-wrapper" id="navigation">
            <div class="navbar navbar-default navbar-fixed-top reveal-menu-home" role="navigation">
                <div class="container nav-container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <div class="logo">
                                    <a href="#">
                                        <img src="../../images/logo_foodcheck.svg" class="foodcheck_logo" alt="foodcheck logo">
                                    </a>
                                </div>
                            </div> <!-- .navbar-header -->
                            <div id="navbar-spy" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="#main">ГЛАВНАЯ</a></li>
                                    <li><a href="#our_target">НАША ЦЕЛЬ</a></li>
                                    <li><a href="#contacts">КОНТАКТЫ</a></li>
                                </ul>
                            </div>
                        </div> <!-- .col-md-12 -->
                    </div> <!-- .row -->
                </div> <!-- .container -->
            </div> <!-- .navbar -->
        </div> <!-- .navbar-wrapper -->
    </nav>
    <!-- End Navigation -->
</header>