<!-- Footer -->
<footer id="contacts" class="page-footer font-small indigo">

    <div class="container">
        <form id="contact_form" class="contact100-form validate-form">
				<span class="contact100-form-title">
					Обратная связь
				</span>

            <div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate="Имя обязательное поле">
                <span class="label-input100">Имя</span>
                <input class="input100" type="text" name="name" placeholder="Введите ваше имя">
                <span class="focus-input100"></span>
            </div>

            <div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate="Валидный email: ex@abc.xyz">
                <span class="label-input100">Email</span>
                <input class="input100" type="text" name="email" placeholder="Введите ваш email">
                <span class="focus-input100"></span>
            </div>

            <div class="wrap-input100 validate-input" data-validate = "Сообщение обязательное поле">
                <span class="label-input100">Сообщение</span>
                <textarea class="input100" name="message" placeholder="Ваше сообщение..."></textarea>
                <span class="focus-input100"></span>
            </div>

            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

            <div class="container-contact100-form-btn">
                <button type="submit" class="contact100-form-btn">Отправить</button>
            </div>
        </form>
    </div>

    <!-- Footer Links -->
    <div class="container">

        <hr class="rgba-white-light" style="margin: 0 15%;">

        <!-- Grid row-->
        <div class="row text-center d-flex justify-content-center pt-5 mb-3">



            <!-- Grid column -->
            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="#">ГЛАВНАЯ</a>
                </h6>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="#our_target">НАША ЦЕЛЬ</a>
                </h6>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-2 mb-3">
                <h6 class="text-uppercase font-weight-bold">
                    <a href="#contacts">КОНТАКТЫ</a>
                </h6>
            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row-->
        <hr class="rgba-white-light" style="margin: 0 15%;">

        <div class="row d-flex text-center justify-content-center mb-md-0 mb-4">
            <div class="col-md-8 pt-5">
                <p style="line-height: 1.7rem; font-size: 1.2rem">
                    Foodcheck - проект независимой молодой команды разработчиков и идеологов,
                    цель которой сделать жизнь в большом городе еще приятнее и качественнее.
                    Мы как настоящие ценители фуд арт сделаем Ваши будни вкусными.
                    Следите за статусом развития проекта в наших социальных сетях.
                </p>
            </div>
        </div>

        <!-- Grid row-->
        {{--<hr class="clearfix d-md-none rgba-white-light" style="margin: 10% 15% 5%;">--}}

        <!-- Grid row-->
        <div class="row pb-3">

            <!-- Grid column -->
            <div class="col-md-12">

                <div class="mb-5 flex-center">

                    <!-- Facebook -->
                    <a class="fb-ic">
                        <i class="fa fa-facebook fa-lg white-text mr-4"> </i>
                    </a>
                    <!-- Twitter -->
                    <a class="tw-ic">
                        <i class="fa fa-twitter fa-lg white-text mr-4"> </i>
                    </a>
                    <!-- Google +-->
                    <a class="gplus-ic">
                        <i class="fa fa-google-plus fa-lg white-text mr-4"> </i>
                    </a>
                    <!--Linkedin -->
                    <a class="li-ic">
                        <i class="fa fa-linkedin fa-lg white-text mr-4"> </i>
                    </a>
                    <!--Instagram-->
                    <a class="ins-ic">
                        <i class="fa fa-instagram fa-lg white-text mr-4"> </i>
                    </a>
                    <!--Pinterest-->
                    <a class="pin-ic">
                        <i class="fa fa-pinterest fa-lg white-text"> </i>
                    </a>

                </div>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row-->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2018 Copyright:
        <a href="#"> foodcheck.com.ua</a>
    </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->