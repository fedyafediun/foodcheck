<div id="main">
   <div class="container">
       <div class="description">
           <h1>Рейтинг доставок суши в Киеве</h1>
           <p>
               Мы готовы предоставить вам платформу, на которой вы сможете понять<br/>
               какой доставкой суши лучше воспользоваться сейчас и не остаться разочарованым.<br/>
           </p>

           <div class="switch switch--vertical switch--no-label"><input id="radio-e" type="radio" name="fourth-switch" checked="checked" /><label for="radio-g">Off</label><input id="radio-f" type="radio" name="fourth-switch" /><label for="radio-h">On</label><span class="toggle-outside"><span class="toggle-inside"></span></span>
           </div>
       </div>
   </div>

    <div class="img1"></div>
    <div class="img2"></div>
    <div class="img3"></div>
    <div class="img4"></div>
</div>