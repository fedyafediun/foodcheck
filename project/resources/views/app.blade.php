<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Наша платформа готова предоставить вам рейтинг доставок суши в Киеве.">
    <meta itemprop="name" content="foodcheck - поможем орпеделить лучшую доставку суши в Киеве">
    <meta itemprop="description" content="Мы готовы помочь вам в выборе лучшей суши в Киеве, вы можете повлиять на формирование лбьективного рейтинга.">
    <meta itemprop="image" content="https://foodcheck.com.ua/images/logo_foodcheck.svg">
    <meta property="og:title" content="FOODCHECK Рейтинг доставок суши в Киеве">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://foodcheck.com.ua">
    <meta property="og:image" content="https://foodcheck.com.ua/images/logo_foodcheck.svg">
    <meta property="og:description" content="Наша платформа готова предоставить вам рейтинг доставок суши в Киеве.">
    <meta property="og:site_name" content="foodcheck">


    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/app.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <title>Foodcheck</title>
</head>
<body>
@include('parts.header')

@include('parts.first_screen')
@include('parts.questions')
@include('parts.sushi_bars')

@include('parts.our_target')
@include('parts.contacts')

<script src="/js/app.js"></script>
<script src="/js/main.js"></script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (m, e, t, r, i, k, a) {
        m[i] = m[i] || function () {
            (m[i].a = m[i].a || []).push(arguments)
        };
        m[i].l = 1 * new Date();
        k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(52086759, "init", {
        id: 52086759,
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true
    });
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/52086759" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
