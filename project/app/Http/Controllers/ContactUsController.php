<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\ContactUsRequest;

class ContactUsController extends Controller
{
    public function __invoke(ContactUsRequest $request)
    {
        $contact = new Contact();
        $contact->fill($request->all());
        $contact->save();

        return response('Success!');
    }
}
