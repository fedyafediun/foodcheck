# Makefile for managing APJ development environment
# Anderson Brandao (andersonbn.itpro@gmail.com)

DOCKER_COMPOSE_CONFIG_FILE=docker-compose.yml
HOST_IP=`ipconfig getifaddr en0`
HOST_IP=`ifconfig | grep 'inet addr:192'| cut -d: -f2 | awk '{ print $$1}'`

.PHONY: setup build run
.SILENT: ip

install: set-env setup run update laravel-setup

etc-host:
	echo ${APP_DOMAIN}

setup:
	which docker-compose || sudo curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-`uname -s`-`uname -m`" -o /usr/local/bin/docker-compose; \
	sudo chmod +x /usr/local/bin/docker-compose

	sudo chmod -R 777 project/storage project/bootstrap/cache

down:
	docker-compose down -v

build:
	docker-compose -f $(DOCKER_COMPOSE_CONFIG_FILE) build

clean:
	docker-compose -f $(DOCKER_COMPOSE_CONFIG_FILE) down

logs:
	docker-compose -f $(DOCKER_COMPOSE_CONFIG_FILE)  logs -f

run:
	docker-compose -f $(DOCKER_COMPOSE_CONFIG_FILE) up -d

workspace:
	docker-compose -f $(DOCKER_COMPOSE_CONFIG_FILE) exec workspace bash

ip:
	echo ${HOST_IP}

restart: clean run

set-env:
	[ -f .env ] || cp .env.dist .env
	[ -f project/.env ] || cp project/.env.example project/.env
	[ -f project/.docker.env ] || cp project/.docker.env.example project/.docker.env

upd-env:
	cp .env.dist .env
	cp project/.env.example project/.env
	cp project/.docker.env.example project/.docker.env

composer-install:
	docker-compose exec -T workspace composer install || echo done

composer-dump:
	docker-compose exec -T workspace composer dump || echo done

db-migrate:
	docker-compose exec -T workspace php artisan migrate || echo done

db-refresh:
	docker-compose exec workspace php artisan migrate:fresh || echo done
	docker-compose exec workspace php artisan admin:reset admin || echo done
	docker-compose exec workspace php artisan db:seed || echo done

update: composer-install db-migrate upd-env

ps:
	docker-compose ps

laravel-setup:
	docker-compose exec workspace php artisan key:generate || echo done

codestyle-fix:
	docker-compose exec workspace ./vendor/bin/phpcbf --standard=PSR2 -n ./app ./tests
	docker-compose exec workspace ./vendor/bin/phpcs --standard=PSR2 -n ./app ./tests

ide-helper:
	docker-compose exec workspace php artisan ide-helper:generate || echo done
	docker-compose exec workspace php artisan ide-helper:models || echo done
	docker-compose exec workspace php artisan ide-helper:meta || echo done
